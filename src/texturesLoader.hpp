#ifndef TEXTURESLOADER_HPP
#define TEXTURESLOADER_HPP

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <vector>

enum class TypeTexture {TABLE, BOOST, FREIN, FOND1, FOND2};

class TexturesLoader
{
    private:
        static sf::RenderWindow  * _window;
        static std::vector<sf::Texture *> _textures;
        
    public:
        static void init(sf::RenderWindow * window);
        static sf::Texture& getTexture(const TypeTexture& type);
        static sf::RenderWindow& getWindow();
};




#endif
