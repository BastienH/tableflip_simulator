#include "map.hpp"

#include <cstdlib>
#include <iostream>
#include <ctime>

Map::Map()
:_table(Table(sf::Vector2<float>(W_WINDOW/2 - 50, H_WINDOW - 50), sf::Vector2<float>(100, 50), 10)), _distance(0), _cpt(0), _tableHitted(false), _pressedAngle(false), _pressedStrengh(false), _angle(0), _strengh(100), _sens(1)
{
    srand(time(0));

    addActor(&_table);

    float textureSize = TexturesLoader::getTexture(TypeTexture::FOND1).getSize().x;

    _background.push_back(sf::Sprite(TexturesLoader::getTexture(TypeTexture::FOND1)));
    _background.push_back(sf::Sprite(TexturesLoader::getTexture(TypeTexture::FOND2)));
    _background.push_back(sf::Sprite(TexturesLoader::getTexture(TypeTexture::FOND2)));

    for(int i = 0; i < 3; ++i)
    {
        _background[i].setPosition(sf::Vector2f(i*textureSize, 0));
    }

    //_table.hitTable(20, 100);
}


Obstacle * Map::generateNextObstacle()
{
    int type = rand()%100;
    int size = 50;

    //std::cout << type << std::endl;

    TypeObstacle typeObs;

    if(type >= 50)
    {
        typeObs = TypeObstacle::FREIN;
    }
    else
    {
        typeObs = TypeObstacle::BOOST;
    }

    float height = rand()%(H_WINDOW-size);
    

    return new Obstacle(&_table, sf::Vector2<float>(W_WINDOW, height), sf::Vector2<float>(size, size), typeObs);
}


void Map::tick()
{

    draw();

    if(_tableHitted)
    {
        for(auto& act : _entities)
        {
            act->tick();
        }

        if(_distance/25000 == _cpt + 1)
        {
            Obstacle * obs = generateNextObstacle();
            addActor(obs);
            ++_cpt;
        }

        _distance += _table.getSpeed().x;

        
        std::cout << _distance << std::endl;
    }
    else
    {
        _table.draw();

        chooseAngleAndForce();
    }
    


}

void Map::draw()
{
    for(auto& sprite : _background)
    {
        if(sprite.getPosition().x +  sprite.getTexture()->getSize().x <= 0)
        {
            sprite.move(sf::Vector2f(3 * sprite.getTexture()->getSize().x - 10, 0));
        }
        
        sprite.move(sf::Vector2f(-1 * _table.getSpeed().x * _table.getTime().asSeconds(), 0));

        TexturesLoader::getWindow().draw(sprite);
    }
}

void Map::chooseAngleAndForce()
{
        sf::RectangleShape bgRectangle(sf::Vector2f(100, 10));

        bgRectangle.setPosition(sf::Vector2f(W_WINDOW/2, H_WINDOW - 25));
        bgRectangle.setFillColor(sf::Color::Black);
        bgRectangle.rotate(_angle);
        TexturesLoader::getWindow().draw(bgRectangle);

        sf::RectangleShape rectangle(sf::Vector2f(_strengh, 10));

        rectangle.setPosition(sf::Vector2f(W_WINDOW/2, H_WINDOW - 25));
        rectangle.setFillColor(sf::Color::Red);
        rectangle.rotate(_angle);
        TexturesLoader::getWindow().draw(rectangle);

        if(!_pressedAngle)
        {

            if(_angle == -90 || _angle == 0)
            {
                _sens *= -1;
            }

            _angle += _sens;
            
        }
        else
        {
            if(!_pressedStrengh)
            {
                if(_strengh == 100 || _strengh == 0)
                {
                    _sens *= -1;
                }
                _strengh += _sens;
            }
            else
            {
                _tableHitted = true;
                _table.hitTable(_angle, _strengh);
            }
        }
        


}

void Map::getEvent()
{
    sf::Event event;

    while (TexturesLoader::getWindow().pollEvent(event))
    {
        // fermeture de la fenêtre lorsque l'utilisateur le souhaite
        switch (event.type)
        {
            // fenêtre fermée
            case sf::Event::Closed:
                TexturesLoader::getWindow().close();
                break;

            case sf::Event::KeyReleased:
                if(event.key.code == sf::Keyboard::Space)
                {
                    if(!_pressedAngle)
                    {
                        _pressedAngle = true;
                        _strengh = 0;
                        _sens = -1;
                    }
                    else
                    {
                        _pressedStrengh = true;
                    }
                }
                break; 
                
            // on ne traite pas les autres types d'évènements
            default:
                break;
        }
    }

}