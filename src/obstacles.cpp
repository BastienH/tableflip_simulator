#include "obstacles.hpp"

Obstacle::Obstacle(Table * table, const sf::Vector2<float>& pos, const sf::Vector2<float>& dim, const TypeObstacle& type)
: Collidable(pos, dim), _type(type), _table(table), _enable(true)
{
    sf::Texture texture;

    if(type == TypeObstacle::BOOST)
    {
        _speedModifier = sf::Vector2<float>(2, 1.25);
        texture = TexturesLoader::getTexture(TypeTexture::BOOST);
        _sprite.setTexture(TexturesLoader::getTexture(TypeTexture::BOOST));
    }
    else
    {
        _speedModifier = sf::Vector2<float>(0.5, 0.75);
        texture = TexturesLoader::getTexture(TypeTexture::FREIN);
        _sprite.setTexture(TexturesLoader::getTexture(TypeTexture::FREIN));
    }

    
    _sprite.setScale(sf::Vector2f(dim.x / texture.getSize().x, dim.y / texture.getSize().y));
    _sprite.setPosition(pos);
    
}

void Obstacle::init()
{

}

void Obstacle::tick()
{
    if(_enable)
    {
        if(isColliding(_table))
        {
            touchTable();
            _enable = false;
        }
        else
        {
            move(sf::Vector2<float>(-_table->getSpeed().x, 0));
            draw();
        }

    }

    
}

void Obstacle::touchTable()
{   
    sf::Vector2<float> speed = _table->getSpeed();


    _table->setSpeed(sf::Vector2<float>(speed.x * _speedModifier.x, speed.y * _speedModifier.y));
    

    move(sf::Vector2<float>(-1 * _table->getSpeed().x, 0));
    
}