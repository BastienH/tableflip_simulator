#ifndef ACTOR_HPP
#define ACTOR_HPP

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

const int W_WINDOW = 1000;
const int H_WINDOW = 450;

class Actor
{
    public:
        virtual void init() = 0;

        virtual void tick() = 0;

};



#endif