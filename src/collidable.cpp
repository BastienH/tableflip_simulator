#include "collidable.hpp"



Collidable::Collidable(const sf::Vector2<float>& pos, const sf::Vector2<float>& dim)
: _position(pos), _dimension(dim), _enable(true)
{
}

bool Collidable::isColliding(Collidable * obj)
{
    bool col =  !(( _position.x + _dimension.x   < obj->getPosition().x)
               || ( _position.x                  > obj->getPosition().x + obj->getDimension().x)
               || ( _position.y + _dimension.y   < obj->getPosition().y)
               || ( _position.y                  > obj->getPosition().y + obj->getDimension().y));

    return col;

}

void Collidable::move(const sf::Vector2<float>& movement)
{
    _dt = _deltaClock.restart();

    _position += (movement * _dt.asSeconds());
    _sprite.move(sf::Vector2<float>(movement.x * _dt.asSeconds(), movement.y * _dt.asSeconds()));

}

void Collidable::draw()
{
    TexturesLoader::getWindow().draw(_sprite);
}