#ifndef TABLE_HPP
#define TABLE_HPP

#include "collidable.hpp"

class Table : public Collidable
{
    private:
        int _weight;
        sf::Vector2<float> _speed;

    public:

        Table(const sf::Vector2<float>& pos, const sf::Vector2<float>& dim, const int& weight);

        const int& getWeight() const { return _weight;} 

        void setWeight(const int& weight){ _weight = weight;}

        const sf::Vector2<float>& getSpeed() const { return _speed;}

        void setSpeed(const sf::Vector2<float>& speed){ _speed = speed;}

        void modifySpeed(const sf::Vector2<float>& speedModifier);

        void hitTable(const float& angle, const float& strenght);

        void addGravity();

        void init();

        void tick();
};




#endif