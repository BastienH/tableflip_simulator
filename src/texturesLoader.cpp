#include "texturesLoader.hpp"

#include <stdexcept>

#include <iostream>

sf::RenderWindow * TexturesLoader::_window = nullptr;
std::vector<sf::Texture *> TexturesLoader::_textures = std::vector<sf::Texture *>(10);

void TexturesLoader::init(sf::RenderWindow * window)
{
    _window = window;

    //load des textures
    _textures[(int)TypeTexture::TABLE] = new sf::Texture();
    _textures[(int)TypeTexture::TABLE]->loadFromFile("../assets/table.png");

    _textures[(int)TypeTexture::BOOST] = new sf::Texture();
    _textures[(int)TypeTexture::BOOST]->loadFromFile("../assets/up.png");

    _textures[(int)TypeTexture::FREIN] = new sf::Texture();
    _textures[(int)TypeTexture::FREIN]->loadFromFile("../assets/down.png");

    _textures[(int)TypeTexture::FOND1] = new sf::Texture();
    _textures[(int)TypeTexture::FOND1]->loadFromFile("../assets/fond1.png");

    _textures[(int)TypeTexture::FOND2] = new sf::Texture();
    _textures[(int)TypeTexture::FOND2]->loadFromFile("../assets/fond2.png");

}

sf::Texture& TexturesLoader::getTexture(const TypeTexture& type)
{
    if((int)type >= (int)_textures.size())
    {
        throw std::out_of_range("type inexistant");
    }

    return *(_textures[(int)type]);
}

sf::RenderWindow& TexturesLoader::getWindow()
{
    return *_window;
}

