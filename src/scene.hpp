#ifndef SCENE_HPP
#define SCENE_HPP

#include "actor.hpp"
#include <vector>

class Scene
{
    protected:
        std::vector<Actor *> _entities;
    public:
        void addActor(Actor * actor){_entities.push_back(actor);};

        void init()
        {
            for(auto& act : _entities)
            {
                act->init();
            }
        }

        virtual void tick()
        {
            for(auto& act : _entities)
            {
                act->tick();
            }
        }

};




#endif