#include "table.hpp"

#include <cmath>
#include <iostream>

Table::Table(const sf::Vector2<float>& pos, const sf::Vector2<float>& dim, const int& weight)
: Collidable(pos, dim), _weight(weight), _speed(sf::Vector2<float>(0, 0))
{
    sf::Texture texture = TexturesLoader::getTexture(TypeTexture::TABLE);

    _sprite.setTexture(TexturesLoader::getTexture(TypeTexture::TABLE));
    
    _sprite.setScale(sf::Vector2f(dim.x / texture.getSize().x, dim.y / texture.getSize().y));
    _sprite.setPosition(pos);
}


void Table::modifySpeed(const sf::Vector2<float>& speedModifier)
{
    _speed += speedModifier;
}

void Table::hitTable(const float& angle, const float& strenghtPrc)
{
    float strenght = strenghtPrc * 50;

    sf::Vector2<float> newSpeed;
    float angleRad = angle * 3.1415 / 180;

    newSpeed.x = strenght / _weight * cos(angleRad);
    newSpeed.y = -1 * strenght / _weight * sin(angleRad);

    modifySpeed(newSpeed);
}

void Table::addGravity()
{
    modifySpeed(sf::Vector2<float>(0, 9.81/_weight));
}

void Table::init()
{

}

void Table::tick()
{

    //std::cout << _speed.x << std::endl;

    static float maxSpeed = 0;
    static int cpt = 0;
    static bool rebound = false;


    addGravity();  

    if(rebound)
    {
        _speed.y = -1 * _speed.y + abs(_speed.y)/10;
        _speed.x = 0.8*_speed.x;

        if(_speed.x < 0.5)
            _speed.x = 0;

        maxSpeed = 0;

        rebound = false;
    }

    if(getPosition().y + getDimension().y + _speed.y*getTime().asSeconds() >= H_WINDOW)
    {
        move(sf::Vector2<float>(0, H_WINDOW - getPosition().y - getDimension().y));      
        rebound = true;
    }
    else
    {
        move(sf::Vector2<float>(0, _speed.y));
    }
    



    if(abs(_speed.y) > maxSpeed)
        maxSpeed = abs(_speed.y);

    if(maxSpeed < 1)
    {
        ++cpt;
        if(cpt >= 10)
        {
            _speed.y = 0;
        }

    }
    else
    {
        cpt = 0;
    }

    //std::cout << _speed.y << std::endl;
    

    draw();

}