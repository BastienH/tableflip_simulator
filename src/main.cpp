#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>


#include "map.hpp"

#include <iostream>



int main(int, char**)
{

    //création de la fenêtre
    sf::RenderWindow window(sf::VideoMode(W_WINDOW, H_WINDOW), "Tableflip Simulator");

    //60 fps
    window.setFramerateLimit(60);

    TexturesLoader::init(&window);

    Map map;

    // on fait tourner le programme tant que la fenêtre n'a pas été fermée
    while (window.isOpen())
    {

        map.getEvent();
        
        // effacement de la fenêtre en noir
        window.clear(sf::Color::Black);

        map.tick();

        // fin de la frame courante, affichage de tout ce qu'on a dessiné
        window.display();
    }

    return 0;
}
