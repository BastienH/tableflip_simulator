#ifndef MAP_HPP
#define MAP_HPP

#include "table.hpp"
#include "obstacles.hpp"
#include "texturesLoader.hpp"
#include "scene.hpp"

#include <vector>

class Map : public Scene
{
    private:
        Table _table;
        unsigned long _distance;
        unsigned long _cpt;
        std::vector<sf::Sprite> _background;
        bool _tableHitted;

        bool _pressedAngle;
        bool _pressedStrengh;
        float _angle;
        float _strengh;
        int _sens;

    public:
        Map();
        Obstacle * generateNextObstacle();
        
        void tick();

        void draw();

        void chooseAngleAndForce();

        void getEvent();


};





#endif
