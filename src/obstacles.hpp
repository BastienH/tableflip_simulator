#ifndef OBSTACLE_HPP
#define OBSTACLE_HPP

#include "collidable.hpp"
#include "table.hpp"

enum class TypeObstacle {BOOST, FREIN};

class Obstacle : public Collidable
{
    private:
        sf::Vector2<float> _speedModifier;
        TypeObstacle _type;
        Table * _table;
        bool _enable;
    public:
        Obstacle(Table * table, const sf::Vector2<float>& pos, const sf::Vector2<float>& dim, const TypeObstacle& type);
       
        void init();

        void tick();

        void touchTable();
};



#endif