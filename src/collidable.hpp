#ifndef COLLIDABLE_HPP
#define COLLIDABLE_HPP

#include "actor.hpp"
#include "texturesLoader.hpp"

class Collidable : public Actor
{
    private:
        sf::Vector2<float> _position;
        sf::Vector2<float> _dimension;
        sf::Clock _deltaClock;
        sf::Time _dt;
    protected:
        bool _enable;
        sf::Sprite _sprite;

    public:
        Collidable(const sf::Vector2<float>& pos, const sf::Vector2<float>& dim);

        bool isColliding(Collidable * obj);

        const sf::Vector2<float>& getPosition() const { return _position;}

        void setPosition(const sf::Vector2<float>& position){ _position = position;}

        const sf::Vector2<float>& getDimension() const { return _dimension;}

        void setDimension(const sf::Vector2<float>& dimension){ _dimension = dimension;}

        sf::Clock& getClock() {return _deltaClock;}

        sf::Time& getTime() {return _dt; }

        virtual void init() = 0;

        virtual void tick() = 0;

        void move(const sf::Vector2<float>& movement);

        void draw();



    
};




#endif


